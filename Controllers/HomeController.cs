

using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using hello_blog_ui.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace hello_blog_ui.Controllers
{
    public class HomeController : Controller
    {
        private const string requestUri = "http://localhost:5000/api/v1/BlogPost";
        public async Task<IActionResult> Index()
        {
            List<BlogPost> blogPosts = new List<BlogPost>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(requestUri))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    blogPosts = JsonConvert.DeserializeObject<List<BlogPost>>(apiResponse);
                }
            }
            return View(blogPosts);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(BlogPost input)
        {
            if (ModelState.IsValid)
            {
                var payload = JsonConvert.SerializeObject(input);
                HttpContent content = new StringContent(payload, Encoding.UTF8, "application/json");
                using (var httpClient = new HttpClient())
                {
                    await httpClient.PostAsync(requestUri, content);
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return View();
            }
        }
    }
}